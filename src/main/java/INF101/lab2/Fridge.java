package INF101.lab2;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge {
    List<FridgeItem> fridge = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return fridge.size();
    }
    
    @Override
    public int totalSize() {
        return 20;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < totalSize()) {
            fridge.add(item);
            return true;
        } else {
            return false;
            }
    }

    @Override
    public void takeOut(FridgeItem item) {
        List<FridgeItem> trash = new ArrayList<>();
        if (fridge.contains(item)) {
            for (int i = 0; i < nItemsInFridge(); i++) {
                if (fridge.get(i).equals(item)) {
                    trash.add(item);
                }
            }
            fridge.removeAll(trash);
        } else {
            throw new NoSuchElementException();
        }
    }

    @Override
    public void emptyFridge() {
        fridge.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem> expiredItems = new ArrayList<>();
        for (int i = 0; i < nItemsInFridge(); i++) {
            if (fridge.get(i).hasExpired()) {
                expiredItems.add(fridge.get(i));
            }
        }
        fridge.removeAll(expiredItems);
        return expiredItems;
    }
}
